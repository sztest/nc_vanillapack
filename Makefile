SRC=../nodecore

update:
	cp "${SRC}/.cdb-release.jpg" .

	rm -rf .tmp
	mkdir -p .tmp
	curl https://gitlab.com/sztest/nodecore/-/archive/dev/nodecore-dev.tar.gz | ( cd .tmp && tar xzf - )

	rm -f *.png
	set -ex; find .tmp -name src -prune -or -type f -name '*.png' -print \
		| while read x; do ln -f "$$x" .; done

	rm -rf .tmp
